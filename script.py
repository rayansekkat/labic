#!/usr/bin/python
import requests
import json
import time
import xml.etree.cElementTree as ET

# ___Edit below___ #
DEVICE_NAME = "test-device" # type the endpoint name of your device.
INSTALLATION_URL = "https://lwm2m-test.avsystem.io" # provide the URL of your Coiote DM installation.
INSTALLATION_API_PORT = "8087" # provide the port for communication with the API. The default value is `8087`.
CREDENTIALS=('user_login', 'password') # provide user name and password of your Coiote DM user account.
TEST_NAMES = { # type the names of the test cases that you want to execute on the device.
        "testCases":[
                "protocol_test_1",
                "protocol_test_2",
                "protocol_test_3",
                "protocol_test_4",
                "protocol_test_5",
        ]
}
# ___Edit above___ #

SCHEDULE_URL = INSTALLATION_URL + ":" + INSTALLATION_API_PORT + "/api/coiotedm/v3/protocolTests/schedule/device/" + DEVICE_NAME
REPORT_URL = INSTALLATION_URL + ":" + INSTALLATION_API_PORT + "/api/coiotedm/v3/protocolTests/report/device/" + DEVICE_NAME
PARAMS = {
        'accept' : 'application/json',
        'Content-Type': 'application/json'
}

root = ET.Element("testsuite")

result = requests.post(url=SCHEDULE_URL, json=TEST_NAMES, auth=CREDENTIALS, params=PARAMS)
if result.status_code != 201:
       print('Could not schedule the tests.')
       print('Server returned: ' + str(result.status_code))
       print('Error message: ' + str(result.json()['error']))
       exit(1)

tests_running = True
while tests_running:
        result = requests.post(url=REPORT_URL, json=TEST_NAMES, auth=CREDENTIALS, params=PARAMS)
        if result.status_code != 200:
                print('Could not read the tests status.')
                print('Server returned: ' + str(result.status_code))
                print('Error message: ' + str(result.json()['error']))
                exit(1)
        tests_running = result.json()["waitingForExecution"]
        time.sleep(15)

for test in result.json()['failed']:
    a = ET.SubElement(root, "testcase", classname="interop", name=test)
    ET.SubElement(a, "failure", type="failure")

for test in result.json()['passedWithWarning']:
    b = ET.SubElement(root, "testcase", classname="interop", name=test)
    ET.SubElement(b, "failure", type="warning")

for test in result.json()['passedSuccessfully']:
    ET.SubElement(root, "testcase", classname="interop", name=test)

tree = ET.ElementTree(root)
tree.write("report.xml")
